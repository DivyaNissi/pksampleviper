//
//  CityDetailWireFrame.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityDetailWireFrame: CityDetailWireFrameProtocol {
    
    class func createCityDetailModule(with cityDetailRef: CityDetailView, and city: City) {
        let presenter = CityDetailPresenter()
        presenter.city = city
        cityDetailRef.presenter = presenter
        cityDetailRef.presenter?.view = cityDetailRef
        cityDetailRef.presenter?.wireframe = CityDetailWireFrame()
    }
    
    func goBackToCityListView(from view: UIViewController) {
        
    }
    
    deinit {
        print("CityDetailWireFrame removed")
    }

}
