//
//  CityListView.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityListView: UIViewController,CityListViewProtocol {
        
        @IBOutlet var cityTblView: UITableView!
        
        var presenter:CityListPresenterProtocol?
        var cityList = [City]()
        override func viewDidLoad() {
            super.viewDidLoad()
            self.title = "CITY LIST"
            CityListWireframe.createCityListModule(cityListRef: self)
            presenter?.viewDidLoad()
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }

        func showCity(with city: [City]) {
            cityList = city
            cityTblView.reloadData()
        }
    }

    extension CityListView: UITableViewDataSource, UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CityListCell.self), for: indexPath) as? CityListCell {
                cell.populateCitiesInfo(cityList[indexPath.row])
                return cell
            }
            return UITableViewCell()
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return cityList.count
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            presenter?.showCitySelection(with: cityList[indexPath.row], from: self)
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return 90
         }
    }


