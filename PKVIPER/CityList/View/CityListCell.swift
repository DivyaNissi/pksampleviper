//
//  CityListCell.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityListCell: UITableViewCell {
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var populationLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func populateCitiesInfo(_ city: City) {
        cityNameLabel?.text = city.name
        let color: UIColor = #colorLiteral(red: 0.8414117518, green: 0.06093361637, blue: 0.5584653756, alpha: 1)
        cityNameLabel?.textColor = color
        populationLable?.text = city.population
        let image : UIImage = UIImage(named: city.name)!
        leftImageView?.image = image
        leftImageView?.layer .cornerRadius = 30
        leftImageView.layer.borderColor = #colorLiteral(red: 0.8414117518, green: 0.06093361637, blue: 0.5584653756, alpha: 1)
        leftImageView.layer.borderWidth = 2.0
        }
}
