//
//  City.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

struct City {
    var name: String!
    var population: String!
    var image: String?
    var description: String?

    init(attributes: [String: String]) {
        self.name = attributes["name"]
        self.population = attributes["population"]
        self.image = attributes["image"]
        self.description = attributes["description"]
    }
}
