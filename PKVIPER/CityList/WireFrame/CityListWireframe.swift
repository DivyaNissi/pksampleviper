//
//  CityListWireframe.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityListWireframe: CityListWireFrameProtocol {

   
    func pushToCityDetail(with city: City,from view: UIViewController) {
        let cityDetailViewController = view.storyboard?.instantiateViewController(withIdentifier: "CityDetailView") as! CityDetailView
        CityDetailWireFrame.createCityDetailModule(with: cityDetailViewController, and: city)
        view.navigationController?.pushViewController(cityDetailViewController, animated: true)
    }
    
    class func createCityListModule(cityListRef: CityListView) {
       let presenter: CityListPresenterProtocol & CityListOutputInteractorProtocol = CityListPresenter()
        cityListRef.presenter = presenter
        cityListRef.presenter?.wireframe = CityListWireframe()
        cityListRef.presenter?.view = cityListRef
        cityListRef.presenter?.interactor = CityListInteractor()
        cityListRef.presenter?.interactor?.presenter = presenter
    }
    
}
