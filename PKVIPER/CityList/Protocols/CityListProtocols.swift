//
//  CityListViewProtocol.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit


protocol CityListViewProtocol: class {
    // PRESENTER -> VIEW
    func showCity(with city: [City])
}

protocol CityListPresenterProtocol: class {
    //View -> Presenter
    var interactor: CityListInputInteractorProtocol? {get set}
    var view: CityListViewProtocol? {get set}
    var wireframe: CityListWireFrameProtocol? {get set}

    func viewDidLoad()
    func showCitySelection(with city: City, from view: UIViewController)
}

protocol CityListInputInteractorProtocol: class {
    var presenter: CityListOutputInteractorProtocol? {get set}
    //Presenter -> Interactor
    func getCityList()
}

protocol CityListOutputInteractorProtocol: class {
    //Interactor -> Presenter
    func cityListDidFetch(cityList: [City])
}

protocol CityListWireFrameProtocol: class {
    //Presenter -> Wireframe
    func pushToCityDetail(with city: City,from view: UIViewController)
    static func createCityListModule(cityListRef: CityListView)
}
