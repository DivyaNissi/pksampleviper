//
//  CityDetailView.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityDetailView: UIViewController, CityDetailViewProtocol {

    @IBOutlet var cityImage: UIImageView!
    @IBOutlet var cityNameLbl: UILabel!
    @IBOutlet var population: UILabel!
    @IBOutlet var discriptionTxtView: UITextView!

    var presenter: CityDetailPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "leftArrow"), style: .plain, target: self, action: #selector(back(sender:)))
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        if var textAttributes = navigationController?.navigationBar.titleTextAttributes {
            textAttributes[NSAttributedString.Key.foregroundColor] = UIColor.white
            navigationController?.navigationBar.titleTextAttributes = textAttributes
        }

        presenter?.viewDidLoad()
    }
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated:true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showCityDetail(with city: City) {
        title = city.name
        cityImage.image = UIImage(named: city.name)
        cityNameLbl.text = city.name
        population.text = city.population
        discriptionTxtView.text = city.description
    }
    
    deinit {
        print("CityDetailView removed")
    }

}
